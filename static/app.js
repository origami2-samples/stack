angular
.module(
  'sample',
  [
    'ng',
    'btford.socket-io'
  ]
)
.run(function ($rootScope, socketFactory) {
  var socket = socketFactory();
  
  var Crane = require('origami-crane');
  var Client = require('origami-client');
  var RSASocket = require('origami-rsa-socket');
  var NameRegistry = require('origami-name-registry');
  var crypto = require('origami-crypto-utils');
  var ClientInitializer = require('origami-client-initializer');
  var TokenProviderClient = require('origami-token-provider-client');
  var TokenProviderClientInitializer = require('origami-token-provider-client-initializer');
  
  var key = localStorage.clientKey;
  
  if (!key) {
    key = crypto.randomKey(512);
    localStorage.clientKey = key;
  }
  
  socket.on('connect', function () {
    (new RSASocket(localStorage.clientKey))
    .connect(socket)
    .then(function (secureSocket) {
      var crane = new Crane(
        new NameRegistry(key),
        function () {
          // no incoming sockets
          return Promise.reject();
        },
        secureSocket
      );
      
      crane
      .createSocket(
        '',
        new TokenProviderClientInitializer(function (clientFactory) {
          var client = clientFactory(); // no token 
          
          client.
          TokenProvider1.getToken(prompt('username?', 'user1'))
          .then(function (token) {
            crane
            .createSocket(
              '',
              new ClientInitializer(function (clientFactory) {
                var client = clientFactory(token);   
                
                client.Plugin2.echo('hello')
                .then(function (response) {
                  alert(response);
                  
                });
              })
            );
          });
        })
      );
    });
  });
});