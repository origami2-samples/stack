var EventEmitter = require('events').EventEmitter;

require('async')
.autoInject(
  {
    publicKey: function (privateKey, callback) {
      var crypto = require('origami-crypto-utils');
      
      callback(null, crypto.asString(crypto.asPrivateKey(privateKey)));
    },
    privateKey: function (callback) {
      callback(null, '-----BEGIN RSA PRIVATE KEY-----\nMIIBPAIBAAJBANTuAVO3T8W0zvEMVvaeOrV8JOdXsIYndQhbQLO1OJ+/ZGvo8VrZ\nnVX5BtIOlWzhei9gEDL/pajDsF6BhrsXO+kCAwEAAQJBAM3HVjD5r3Z6TqRWMJUW\nRdauq1uIO2jrKQdyaQ1Dzf1SzlbmTAP2nSOn6J/TPUmQVpiOLwj3soUfXrRMbODi\njAECIQD27oHXpwgC13sMwf2mnX3vpq2o9bL5gm5DWXj5lHwIUQIhANy/1VpEaZNn\nSYLoV7HOVNPukREqc/pyYQ0ZCVKaJKwZAiEAoBk1cBeo1wbUjgn8phk4fLfpokFi\n/+i0CtCo4dCGtnECIQDWjTi/aDi4xK3FJy98qI74ASpL5dgddifvxAK0nw6/mQIg\nMwKOLnqLPRJ/IbqB9XTUSUWtdihdsaCGYbcO780siUM=\n-----END RSA PRIVATE KEY-----');
    },
    app: function (callback) {
      var app = require('express')();
      
      callback(null, app);
    },
    server: function (app, callback) {
      var server = require('http').createServer(app);
      
      callback(null, server);
    },
    stack: function (server, privateKey, publicKey, callback) {
      var StackIO = require('origami-stack-io');
      
      var stack = new StackIO(privateKey);
      
      setInterval(function () {
        stack.getStack().publish('my-event', {'hello': 'there'});
      }, 2000);
      
      stack.getStack().authorizeGuardPublicKey('guard1', publicKey);
      
      stack.getStack().addGuard('guard1', new EventEmitter());
      stack.getStack().addGuardCheck('guard1', 'check1', `
        if (method === 'onlyAdmin' && !admin) return reject();
      `)
      
      stack.listen(server);
      stack.authorizePlugin('Plugin1',publicKey);
      stack.authorizePlugin('Plugin2',publicKey);
      stack.authorizeTokenProvider('TokenProvider1', publicKey);
      
      callback(null, stack);
    },
    clientBundle: function (callback) {
      var browserify = require('browserify');

      var clientBundler = browserify(
        [],
        {
          require: [
            'origami-crane',
            'origami-token-provider-client-initializer',
            'origami-token-provider-client',
            'origami-client',
            'origami-client-initializer',
            'origami-name-registry',
            'origami-rsa-socket',
            'origami-crypto-utils'
          ]
        }
      );
      
      clientBundler
      .bundle(function (err, buffer) {
        if (err) return callback(err);
        
        callback(null, buffer.toString());
      });
    }
  },
  function (err, results) {
    if (err) throw err;
    
    results.app.get('/bundle.js', function (req, res) {
      res.send(results.clientBundle).end();
    });
    
    results.app.use(require('serve-static')(require('path').join(__dirname, './static')));
    
    results.server.listen(4000);
  }  
);
